//
//  main.m
//  AutoLayoutDemo
//
//  Created by Ido Franindo on 9/28/15.
//  Copyright (c) 2015 Ido Franindo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
